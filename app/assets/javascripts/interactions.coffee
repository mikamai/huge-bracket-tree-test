class RiotInteractions
  constructor: ->
    @bindEvents()

  bindEvents: ->
    self = this

    $('#container div h5:nth-child(1)').on 'mouseenter', (event) ->
      self.checkHover(this)

    .on 'mouseleave', (event) ->
      $('#t').stop(true).fadeOut(250)
      self.removeHighlight(this)

    $('#container div h5:nth-child(2)').on 'mouseenter', (event) ->
      self.checkHover(this)

    .on 'mouseleave', (event) ->
      $('#t').stop(true).fadeOut(250)
      self.removeHighlight(this)

    $('#go-fullscreen').on 'click', (event) =>
      event.preventDefault()
      @goFullscreen()

    $(document).on 'webkitfullscreenchange fullscreenchange mozfullscreenchange', ->
      $('#brackets-wrapper').toggleClass 'fullscreen-brackets'

  openPopUp: (el) ->
    $('#t').stop(true).fadeIn(250)
    @.highlightTeamName(el)
    element = $(el).offset()
    @setPopUpPos element.left, element.top


  setPopUpPos: (posX, posY) ->
    $('#t').css({ "left": posX, "top": posY + 25 })

  checkHover: (el) ->
    setTimeout =>
      if $(el).is(':hover') is false
        return
      else
        @openPopUp(el)
    , 500

  highlightTeamName: (el) ->
    $(el).addClass 'highlight'

    if $(el).is(':nth-child(2)')
      $(el).addClass 'squared'

  removeHighlight: (el) ->
    $(el).removeClass 'highlight'

    if $(el).is(':nth-child(2)')
      $(el).removeClass 'squared'

  goFullscreen: ->
    elem = document.getElementById('brackets-wrapper')
    req  = elem.requestFullScreen || elem.webkitRequestFullScreen || elem.mozRequestFullScreen
    req.call elem

$ ->
  new RiotInteractions