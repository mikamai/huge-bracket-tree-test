class HomeController < ApplicationController
  helper_method :teams, :initial_matches, :body_height, :team_height, :steps

  def index
  end

  private

  def teams
    @teams ||= Summoner.all.take(4096)
  end

  def initial_matches
    teams.length / 2
  end

  def team_height
    50
  end

  def team_width
    150
  end

  def team_height_padding
    10
  end

  def steps
    Math.log teams.size, 2
  end

  def body_height
    initial_matches * team_height + (initial_matches - 1) * team_height_padding
  end
end