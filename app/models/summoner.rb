class Summoner
  attr_reader :name

  def initialize name
    @name = name
  end

  def self.all
    @summoners ||= 2048.times.map { Summoner.new "#{adjectives.sample} #{summoner_names.sample}" }
  end

  private

  def self.summoner_names
    @names ||= [ "Aatrox", "Ahri", "Akali", "Alistar", "Amumu", "Anivia", "Annie", "Ashe",
      "Blitzcrank", "Brand", "Caitlyn", "Cassiopeia", "Chogath", "Corki",
      "Darius", "Diana", "Draven", "DrMundo", "Elise", "Evelynn", "Ezreal",
      "FiddleSticks", "Fiora", "Fizz", "Galio", "Gangplank", "Garen", "Gragas",
      "Graves", "Hecarim", "Heimerdinger", "Irelia", "Janna", "JarvanIV", "Jax",
      "Jayce", "Jinx", "Karma", "Karthus", "Kassadin", "Katarina", "Kayle",
      "Kennen", "Khazix", "KogMaw", "Leblanc", "LeeSin", "Leona", "Lissandra",
      "Lucian", "Lulu", "Lux", "Malphite", "Malzahar", "Maokai", "MasterYi",
      "MissFortune", "MonkeyKing", "Mordekaiser", "Morgana", "Nami", "Nasus",
      "Nautilus", "Nidalee", "Nocturne", "Nunu", "Olaf", "Orianna", "Pantheon",
      "Poppy", "Quinn", "Rammus", "Renekton", "Rengar", "Riven", "Rumble",
      "Ryze", "Sejuani", "Shaco", "Shen", "Shyvana", "Singed", "Sion", "Sivir",
      "Skarner", "Sona", "Soraka", "Swain", "Syndra", "Talon", "Taric", "Teemo",
      "Thresh", "Tristana", "Trundle", "Tryndamere", "TwistedFate", "Twitch",
      "Udyr", "Urgot", "Varus", "Vayne", "Veigar", "Vi", "Viktor", "Vladimir",
      "Volibear", "Warwick", "Xerath", "XinZhao", "Yasuo", "Yorick", "Zac",
      "Zed", "Ziggs", "Zilean", "Zyra" ]
  end

  def self.adjectives
    @abjectives ||= [ 'able','abnormal','absent-minded','above average','adventurous',
      'affectionate','agile','agreeable','alert','amazing','ambitious',
      'amiable','amusing','analytical','angelic','apathetic','apprehensive',
      'ardent','artificial','artistic','assertive','attentive','average',
      'awesome','awful','balanced','beautiful','below average','beneficent',
      'blue','blunt','boisterous','brave','bright','brilliant','buff','callous',
      'candid','cantankerous','capable','careful','careless','caustic',
      'cautious','charming','childish','childlike','cheerful','chic','churlish',
      'circumspect','civil','clean','clever','clumsy','coherent','cold',
      'competent','composed','conceited','condescending','confident','confused',
      'conscientious','considerate','content','cool','cool-headed',
      'cooperative','cordial','courageous','cowardly','crabby','crafty',
      'cranky','crass','critical','cruel','curious','cynical','dainty',
      'decisive','deep','deferential','deft','delicate','demonic','dependent',
      'delightful','demure','depressed','devoted','dextrous','diligent',
      'direct','dirty','disagreeable','discerning','discreet','disruptive',
      'distant','distraught','distrustful','dowdy','dramatic','dreary','drowsy',
      'drugged','drunk','dull','dutiful','eager','earnest','easy-going',
      'efficient','egotistical','elfin','emotional','energetic','enterprising',
      'enthusiastic','evasive','even-tempered','exacting','excellent',
      'excitable','experienced','fabulous','fastidious','ferocious','fervent',
      'fiery','flabby','flaky','flashy','frank','friendly','funny','fussy',
      'generous','gentle','gloomy','glutinous','good','grave','great','groggy',
      'grouchy','guarded','hateful','hearty','helpful','hesitant','hot-headed',
      'hypercritical','hysterical','idiotic','idle','illogical','imaginative',
      'immature','immodest','impatient','imperturbable','impetuous',
      'impractical','impressionable','impressive','impulsive','inactive',
      'incisive','incompetent','inconsiderate','inconsistent','independent',
      'indiscreet','indolent','indefatigable','industrious','inexperienced',
      'insensitive','inspiring','intelligent','interesting','intolerant',
      'inventive','irascible','irritable','irritating','jocular','jovial',
      'joyous','judgmental','keen','kind','lame','lazy','lean','leery',
      'lethargic','level-headed','listless','lithe','lively','local','logical',
      'long-winded','lovable','love-lorn','lovely','maternal','mature','mean',
      'meddlesome','mercurial','methodical','meticulous','mild','miserable',
      'modest','moronic','morose','motivated','musical','naive','nasty',
      'natural','naughty','negative','nervous','noisy','normal','nosy','numb',
      'obliging','obnoxious','old-fashioned','one-sided','orderly',
      'ostentatious','outgoing','outspoken','passionate','passive','paternal','paternalistic','patient','peaceful','peevish','pensive','persevering','persnickety','petulant','picky','plain','plain-speaking','playful','pleasant','plucky','polite','popular','positive','powerful','practical','prejudiced','pretty','proficient','proud','provocative','prudent','punctual','quarrelsome','querulous','quick','quick-tempered','quiet'].map(&:capitalize)
  end
end