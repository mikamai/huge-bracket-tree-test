## README

Test application to check how the browser handles a huge tournament bracket with 4096 teams. See the demo on (http://huge-bracket-tree-test.herokuapp.com/)[http://huge-bracket-tree-test.herokuapp.com/].
